#! /bin/bash


# Emmabuntus_create_cache_apt_exec.sh --
#
#   This file permits to create apt cache
#   is the cache is not present for Emmabuntüs distribution.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


########################################################################################################

clear


# Permet de vérifier la présence d'Internet
if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then


########################################################################################################
# Fonction de mise à jour du cache
########################################################################################################

    echo "Internet is a live"

    # Suppression de la surveillance de la mise à jour des paquets
    if [[ $(cat /proc/cmdline | grep -i boot=live) ]] ; then
        pkill pk-update-icon
    fi

    sudo apt-get -qq update &

fi

sudo chmod o+r /var/lib/command-not-found/commands.db*

exit 0
